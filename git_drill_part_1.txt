git_drill_part_1

1. Create a directory called git_sample_project and cd to git_sample_project
$ mkdir git_sample_project
$ cd git_sample_project

2. Initialize a git repo.
$ git init

3. Create the following files a.txt, b.txt and c.txt 
$ touch a.txt b.txt c.txt

4. add arbitary content to above files.
$ echo "# file a" > a.txt
$ echo "# file b" > b.txt
$ echo "# file c" > c.txt

5. Add a.txt and b.txt to the staging area
$ git add a.txt b.txt

6. Run git status. Understand what it says.
$ git status

7. Commit a.txt and b.txt with the message "Add a.txt and b.txt"
-- intermediate steps for first usage --
$ git config user.email "mail-id"
$ git config user.name "user name"
--then--
$ git commit -m "Add a.txt and b.txt"

8. Run git status. Understand what it says.
$ git status
-- displays the untracked files by git --

9.Run git log
$ git log
-- Displays the latest commits with commit message.

10. Add and commit c.txt
$ git add c.txt
$ git commit -m "Adding file c.txt"

11. Create a project on GitLab.
-- create the project and give name for project --
-- follow steps given in the dashboard of the create project to push
the project --

12. Push your code to GitLab
$ git remote add origin https://gitlab.com/aravind.madabhavi/git_drill_part_1.git
$ git push -u origin master

13. Clone your project in another directory called git_sample_project_2
Syntax:  git clone <remote-repository> <name of repo to be on local>
$ git clone https://gitlab.com/aravind.madabhavi/git_drill_part_1.git git_sample_project_2

14. In git_sample_project_2, add a file called d.txt.
$ touch d.txt
$ echo "# file d" > d.txt
$ git add d.txt

15. Commit and push d.txt
$ git commit -m "Adding file d.txt"
$ git push -u origin master

16. cd back to git_sample_project
$ cd ../git_sample_project

17. Pull the changes from GitLab
$ git pull

18. Copy git_drill_part_1.txt to git_sample_project. Commit and Push it on GitLab
$ git add git_drill_part_1.txt
$ git commit -m "Adding git_drill_part_1.txt"
$ git push -u origin master
-- authenticate with GitLab credentials --

19. Copy cli_drill_part_1.txt to git_sample_project. Commit and Push it on GitLab.
$ git add cli_drill_part_1.txt
$ git commit -m "Adding cli_drill_part_1.txt"
$ git push -u origin master
-- authenticate with GitLab credentials --

20. Add santu@mountblue.io and pramod@mountblue.io as "Developers" to your project.
-- using GitLab dashboard to add the members for the project --
Project Dashboard > Settings > Members > Enter Members > Select Role Permission >
 Add to Project